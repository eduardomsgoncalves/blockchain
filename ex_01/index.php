<?php

class Ex01BlockChain {
    private $names = ['Chase', 'Rennie', 'Franklin', 'Huynh', 'England', 'Lugo', 'Rodrigues', 'Betts', 'Cummings', 'Irwin', 'Nixon', 'Higgins', 'Cook', 'Ross', 'Eaton', 'Fountain'];
    private $template = "Olá <var01>. Meu nome é <var02>.";
    private $size = 0;
    private $chain = [];

    public function __construct() {
        $this->size = count($this->names);
    }

    private function writeBlock($nrBlock, $origin, $destiny) {
        $message = "Origem: " . $origin . PHP_EOL . "Destino: " . $destiny . PHP_EOL . "Mensagem: " . str_replace(['<var01>', '<var02>'], [$destiny, $origin], $this->template) . PHP_EOL;
        file_put_contents("blocks/bloco_" . $nrBlock . ".txt", $message);
    }

    private function makeChain() {
        for ($i = 0; $i < $this->size; $i++) {
            $blocks = file_get_contents("blocks/bloco_" . ($i + 1) . ".txt");
            $beforeBlock = (($i > 0) ? file_get_contents("blocks/bloco_" . (($i + 1) - 1) . ".txt") : null);

            $this->chain[] = [
                "Bloco" => $blocks,
                "Hash" => hash("sha256", $blocks),
                "Hash Anterior" => ($i > 0 ? hash("sha256", $beforeBlock) : null),
            ];
        }
    }

    public function makeBlocks() {
        for ($i = 0; $i < $this->size; $i++) {
            $this->writeBlock(($i + 1), $this->names[$i], ($i == ($this->size - 1) ? $this->names[0] : $this->names[($i + 1)]));
        }
        $this->makeChain();
    }

    public function getChain() {
        return $this->chain;
    }

    public function validateChain(array $chain) {
        $size = count($chain);
        for ($i = 0; $i < $size; $i++) {
            $hash = hash("sha256", $chain[$i]["Bloco"]);
            $hashAnterior = ($i > 0 ? hash("sha256", $chain[$i - 1]["Bloco"]) : null);
            if ($hash != $chain[$i]["Hash"]) {
                return [false, "Hash inválida no bloco: " . ($i + 1)];
            }

            if ($hashAnterior != $chain[$i]["Hash Anterior"]) {
                return [false, "Hash Anterior inválida no bloco: " . ($i + 1)];
            }
        }

        return [true, "Chain validada com sucesso"];
    }

}

$blockChain = new Ex01BlockChain();

$blockChain->makeBlocks();
$chains = $blockChain->getChain();

echo "CHAIN: <br><br>";
foreach ($chains as $chain){
    echo "<br>";
    foreach ($chain as $c){
        echo $c;
        echo "<br>";
    }
}
echo "<br>";
echo "Teste sem Alteração ==> ";
list($isValid, $message) = $blockChain->validateChain($chains);
echo $message;

echo "<hr>";
echo "CHAIN: <br><br>";
foreach ($chains as $chain){
    echo "<br>";
    foreach ($chain as $c){
        echo $c;
        echo "<br>";
    }
}
echo "<br>";

echo "Teste Alterado ==> ";
$chains[4]["Bloco"] .= "Alterando conteúdo 5";

list($isValid, $message) = $blockChain->validateChain($chains);
echo $message;

