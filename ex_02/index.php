<?php

class ExBlockChain02 {
    private $names = ['Betts', 'Chase', 'Cook', 'Cummings', 'Eaton', 'England', 'Fountain', 'Franklin', 'Higgins', 'Huynh', 'Irwin', 'Lugo', 'Nixon', 'Rennie', 'Rodrigues', 'Ross'];
    private $size = 0;
    private $messages = [];

    public function __construct() {
        $this->size = count($this->names);
        $this->readMessages();
    }

    private function readMessages() {
        $this->messages = glob('Messages/*.txt');
    }

    public function getMessages() {
        return $this->messages;
    }

    public function getNames() {
        return $this->names;
    }

    public function getRecipient($message) {
        $firstIndex = strpos($message, 'h ');
        $lastIndex = strpos($message, '. ');
        return substr($message, ($firstIndex + 2), ($lastIndex - ($firstIndex + 2)));
    }

    public function getSender($message) {
        $firstIndex = strpos($message, ': ');
        return substr($message, ($firstIndex + 2));
    }

    public function encrypt($data, $key, $type = 'public') {
        if ($type == 'private') {
            $result = openssl_private_encrypt($data, $encrypted, $key);
        } else {
            $result = openssl_public_encrypt($data, $encrypted, $key);
        }

        if ($result) {
            return $encrypted;
        }
    }

    public function decrypt($data, $key, $type = 'public') {
        if ($type == 'private'){
            $result = openssl_private_decrypt($data, $decrypted, $key);
        }else{
            $result = openssl_public_decrypt($data, $decrypted, $key);
        }

        if ($result) {
            return $decrypted;
        }
    }

}

$blockChain = new ExBlockChain02();
$messages = $blockChain->getMessages();
$names = $blockChain->getNames();
foreach ($messages as $message) {
    $msgBase64 = file_get_contents($message);
    $messageRSA = base64_decode($msgBase64);
    foreach ($names as $nameSender) {
        $privateKey = file_get_contents("Keys/{$nameSender}_private_key.pem");
        $publicKey = file_get_contents("Publics/{$nameSender}_public_key.pem");
        if ($messageDecrypted = $blockChain->decrypt($messageRSA, $privateKey, 'private')) {
            echo "FILE: " . $message . "<br>";
            echo "CONTENT: " . $messageDecrypted . "<br>";
            echo "SENDER: " . $blockChain->getSender($messageDecrypted) . "<br>";
            echo "RECIPIENT: " . $blockChain->getRecipient($messageDecrypted) . "<br>";
            echo "REAL SENDER: {$nameSender}<br>";
        }
    }
    echo "<hr>";
}