<?php

require __DIR__ . '/vendor/autoload.php';

$pvtKey = "Here put the private key";
$pubKey = "Here put the public key";
$address = "here put address to make transaction";

\BitWasp\Bitcoin\Bitcoin::setNetwork(\BitWasp\Bitcoin\Network\NetworkFactory::bitcoinTestnet());

$utxo = "PUT UTXO";
$txo = \BitWasp\Bitcoin\Bitcoin::getTransaction($utxo, true);

$addressTransaction25Percent = "mr9xV9aNnfv6a1LhGsvVmFGW63Mn3ZwtJz";
$faucet = "Your Faucet";

$utxoValue = 100000;
$fee = 44 * 526;

$builderTransaction = new \BitWasp\Bitcoin\Transaction\TransactionBuilder(\BitWasp\Bitcoin\Bitcoin::getEcAdapter());
$builderTransaction->spendOutput($utxo, $fee)
    ->addInput(new \BitWasp\Bitcoin\Transaction\TransactionInput($utxo, $utxoValue))
    ->addOutput(new \BitWasp\Bitcoin\Transaction\TransactionOutput($utxoValue * 0.25, $addressTransaction25Percent))
    ->addOutput(new \BitWasp\Bitcoin\Transaction\TransactionOutput($utxoValue * 0.05, $address))
    ->addOutput(new \BitWasp\Bitcoin\Transaction\TransactionOutput((($utxoValue * 0.70) - $fee), $faucet));

$builderTransaction->signInputWithKey($pvtKey, $txo->getOutputs()->getOutput($fee)->getScript(), 0);

\BitWasp\Bitcoin\Bitcoin::sendrawtransaction($builderTransaction, true);
