Bibliote Smart Contracts em PHP: ethereum-php 

Github: https://github.com/digitaldonkey/ethereum-php

//example 01
pragma solidity 0.6.12;

contract MyContract {
    uint256 peopleCount = 0;

    mapping(uint => Person) public people;

    struct Person {
        uint _id;
        string _firstName;
        string _lastName;
    }

    function addPerson(string memory _firstName, string memory _lastName) public {
        peopleCount += 1;
        people[peopleCount] = Person(peopleCount, _firstName, _lastName);
    }
}

//Another contract
pragma solidity 0.6.12;

contract CellSubscription {
	uint256 monthlyCost;

    constructor(uint256 cost) public {
        monthlyCost = cost;
    }

    function makePayment() payable public {

    }

    function withdrawBalance() public {
        msg.sender.transfer(address(this).balance);
    }

    function isBalanceCurrent(uint256 monthsElapsed) public view returns (bool) {
        return monthlyCost * monthsElapsed == address(this).balance;
    }
}